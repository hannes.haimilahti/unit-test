// src/calc.js

/**
 * Adds two numbers together
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * Substract subtrahend from minuend.
 * @param {number} minuend 
 * @param {number} subtrahend 
 * @returns {number} difference
 */
const substract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

/**
 * Multiplies two numbers.
 * @param {number} multiplicand 
 * @param {number} multiplier 
 * @returns {number} product
 */
const multiply = (multiplicand, multiplier) => {
    return multiplicand * multiplier;
}

/**
 * Divides dividend by divisor.
 * @param {number} dividend 
 * @param {number} divisor 
 * @throws {Error} if the divisor is zero.
 * @returns {number} quotient
 */
const divide = (dividend, divisor) => {
    if (divisor === 0) {
        throw new Error("Can not divide by zero!");
    }
    return dividend / divisor;
}

export default { add, substract, multiply, divide };
