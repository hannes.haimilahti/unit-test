// src/main.js

import express from 'express';
import calc from './calc.js';

const PORT = 3000;
const HOST = "localhost";

const app = express();

// Endpoint - GET http://HOST:PORT/
app.get('/', (req, res) => {
    res.send('Hello yellow world!');
});

// Add endpoint - GET http:/HOST:PORT/add
app.get("/add", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.add(a,b).toString());
});

// Substract endpoint - GET http:/HOST:PORT/substract
app.get("/substract", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.substract(a,b).toString());
});

// Multiply endpoint - GET http:/HOST:PORT/multiply
app.get("/multiply", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.multiply(a,b).toString());
});

// Divide endpoint - GET http:/HOST:PORT/divide
app.get("/divide", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.divide(a,b).toString());
});

app.listen(PORT, HOST, () => {
    console.log(`Listening on http://${HOST}:${PORT}`);
});
