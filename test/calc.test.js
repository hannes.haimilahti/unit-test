import calc from '../src/calc.js';
import { expect, assert, should } from 'chai';

describe('Unit testing calc.js', () => {
    before(() => {
        console.log("The testing game begins!");
    });

    it('should add two numbers together.', () => {
        expect(calc.add(1, 2)).to.equal(3);
        expect(calc.add(5, 7)).to.equal(12);
    });
    it('should subtract two numbers', () => {
        assert(calc.substract(3, 2) == 1);
        assert(calc.substract(5, 25) == -20);
    });
    it('should multiply two numbers', () => {
        should().equal(calc.multiply(10, 5), 50);
        should().equal(calc.multiply(-5, -3), 15);
    });
    it('should throw error when divided by zero', () => {
        should().throw(calc.divide.bind(null, 5, 0));
    });
    
    after(() => {
        console.log("So how did it go?");
    });
});
