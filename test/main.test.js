import { expect, assert, should } from 'chai';
import { spawn } from 'child_process';
import { get } from 'http';

describe('Testing main.js API endpoints', () => {
    let server;
    before(async () => {
        console.log("Starting up the server!");
        server = spawn('node', ['./src/main.js']);
        server.stdout.on('data', (data) => {
            console.log("Server: " + data);
        });
        server.stderr.on('data', (data) => {
            console.log("Server: " + data);
        });
        server.on('exit', (code) => {
            console.log(`Server closed with code ${code}.`);
        })

        // Wait a bit for the server to get started
        await new Promise(resolve => setTimeout(resolve, 500));
    });

    /**
     * Sends a semi-synchronous GET request to http://localhost:3000/[endpoint]?a=[a]&b=[b]
     * @param {string} endpoint 
     * @param {number} a 
     * @param {number} b 
     * @returns {number} Response from the server
     */
    const getResult = async (endpoint, a, b) => {
        let address = `http://localhost:3000/${endpoint}?a=${a}&b=${b}`;
        let data = '';

        let prom = new Promise((resolve, reject) => {
            let req = get(address, (resp) => {
                resp.on('data', (chunk) => {
                    data += chunk;
                });
                
                resp.on('end', () => {
                    resolve(data);
                });
            }).on('error', (e) => {
                console.log(e);
            });
        });
        
        await prom;

        return parseInt(data);
    };

    it('should add two numbers together.', async () => {
        expect(await getResult('add', 1, 2)).to.equal(3);
    });
    it('should subtract two numbers', async () => {
        expect(await getResult('substract', 1, 2)).to.equal(-1);
    });
    it('should multiply two numbers', async () => {
        expect(await getResult('multiply', 3, 2)).to.equal(6);
    });
    it('should divide 2 numbers', async () => {
        expect(await getResult('divide', 10, 2)).to.equal(5);
    });
    
    after(() => {
        console.log("Closing the server.");
        server.kill(2);
    });
});
